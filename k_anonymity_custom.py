import pandas as pd

from datetime import datetime

def prepararDados(dataset):

    datasetAnon = dataset.copy()

    datasetAnon['ano_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).year

    datasetAnon['mes_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).month

    datasetAnon['dia_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).day

    del datasetAnon['data_nascimento']

    return datasetAnon

def converterDataset(dataset):

    datasetAnon = prepararDados(dataset)
    
    #datasetAnon.sort_index(inplace=True)

    datasetAnon.sort_values(by=['genero','estado','cidade','ano_nascimento','mes_nascimento','dia_nascimento'], inplace = True);

    return datasetAnon


def separarClasses(dataset, k):
    datasetAnon = converterDataset(dataset)
    r = k-1
    r_old = 0
    r_new = r

    total_rows = len(datasetAnon)

    classes = []
    while (r_new <= total_rows):
        x = datasetAnon[r_old:r_new].copy()
        classes.append(x)
        aux = r_old
        r_old = r_new 
        r_new = r_new + r

        if(r_new > total_rows):
            x = datasetAnon[aux:].copy()
            classes[len(classes)-1] = x
    return classes

def checkEqual(df, size):
    teste = []
    for i in range(0,size):       
        for j in range(0,size):
            if(i != j):
                if(df.iloc[i]['dia_nascimento'] != df.iloc[j]['dia_nascimento']):
                    teste = [False, 'dia_nascimento']
                else:
                    if(df.iloc[i]['mes_nascimento'] != df.iloc[j]['mes_nascimento']):
                        teste = [False, 'mes_nascimento']
                    else:
                        if(df.iloc[i]['ano_nascimento'] != df.iloc[j]['ano_nascimento']):
                            teste = [False, 'ano_nascimento']
                        else:
                                if(df.iloc[i]['cidade'] != df.iloc[j]['cidade']):
                                    teste = [False, 'cidade']
                                    return teste
                                else:
                                    if(df.iloc[i]['estado'] != df.iloc[j]['estado']):
                                        teste = [False, 'estado']
                                        return teste
                                    else:
                                        if(df.iloc[i]['genero'] != df.iloc[j]['genero']):
                                            teste = [False, 'genero']
                                            return teste
                                        else:
                                            #print 'ok'
                                            teste = [True]
    return teste

def generalizar(df, attr, size):
    df.loc[:, attr] = '*'

def anonimizar(dataset, k):
    classes = separarClasses(dataset, k)
    classes_total_rows = len(classes)
    for i in range(0,classes_total_rows):
        size = len(classes[i])

        aux = checkEqual(classes[i], size)
        while( not aux[0] ):
            generalizar(classes[i], aux[1], size)
            aux = checkEqual(classes[i], len(classes[i]))
    return classes

def dataset_Anonimizado(dataset, k):
    classes = anonimizar(dataset, k);
    df = []
    for r in classes:
        for x in r.values.tolist():
            df.append(x)
    headers = ['genero','cidade','estado','doenca','ano_nascimento','mes_nascimento','dia_nascimento']        
    datasetAnon = pd.DataFrame(df, columns=headers)
    return datasetAnon

def k_anonymity(dataset, k):
    return dataset_Anonimizado(dataset, k)
